---
title: "Flight from Flusskasserne"
date: 2020-01-18
tags:
- whitemouse
- session
---

After watching the pretty light of FLusskasserne burning, the party quickly decided that it was a good idea to be somewhere else, because the soldiers were forming up. They headed into the woods on a game trail, where the mounted soldiers would have trouble following.

It quickly became apparent that Elyse was going to have trouble navigating a game trail at night, and their flight was slow.  To avoid being caught they left the trail and struck out overland for the road.  Dawn found them still in the woods, where they decided to make a cold camp and rest.

Near dusk they came out of the wood at a crossroads, between the main road between Einkasserne and River Run, and a nameless track that runs up into the mountains, used by the occasional gobln trader.

As they were making camp for the night and preparing their meal, they saw a crone traveling the main road by herself.  To Aldo's eyes there was something not right about her, but Eltharion understood the importance of the laws of hospitality and invited her to sit by their fire, sharing the meal and the heat of the fire for the night. Antonio bravely avoided seducing her.

In the morning, although the group had kept watch through the night, the crone was gone without anyone seeing her go.  The group decided at that point to leave the road, and they would head into the mountains to establish contact with the goblins.