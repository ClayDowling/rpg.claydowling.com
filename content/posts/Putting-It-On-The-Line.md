---
title: "Putting It On The Line"
categories: whitemouse
tags: ["whitemouse"]
date: 2019-02-02
---

We're just getting to know these characters, so I'd like to start with a little
game that I lifted from [The Ultimate RPG Backstory
Guide](https://amzn.to/2t036ZE).  Your character probably didn't spring fully
formed from the head of Zeus and declare "I'm going to seek danger and
adventure!" Something pushed you into this life, and the thing that pushed you
will tell us a lot about your character.

I'd also like you to start thinking like a story teller, and the story that
you're going to tell about your own characters.  As part of our first session
we're going to tell the story of how you became an adventurer.  This game can
help you get started.

Think about your character's first trip up Shit Creek.  Why were they in this
situation?  How did they get themselves wedged between this particular rock and
hard place?  What consequences did they suffer?

Start by filling out this quiz: [On The
Line](https://goo.gl/forms/skSpOnR8onbcKh813) to think about your reactions to
the trouble.  If you're stumped, this is a multiple choice form, and each item
conveniently has 6 options.  Generating a random backstory is a perfectly
legitimate way to start.

Quizes not your thing?  Just write about the shit you got yourself into in the
comments below.  The important thing isn't filling out a form.  It's
understanding where your character came from, and how they've reacted to the
trouble.
