---
title: "Inn at the River"
tags: ["whitemouse"]
date: 2019-02-21
card: "/images/card/sturgeon-river.jpg"
image: "/images/banner/sturgeon-river.jpg"
---

After a week trapped at the inn at the river, waiting for the torrent to slow so that you could cross, you were introduced to two women, Sieghilde and Saskia, who were wanted by soldiers of Lord Imhoff.

Elyse, in an epic fit of impulsiveness, declared Sieghilde her bestest new blood sister in response to the request to shelter them.  The soldiers, attention captured, immediately recognzied the women they were looking for.  Hijinx ensued.  Antonio was smitten with Sieghilde, Aldo set soldiers on fire, Eltharion did impressive things with swords, and Elyse sang a song to empty men's minds.

After quickly excusing themselves from the inn, Aldo and Antonio retrieved their horses, and everybody else followed a path north along the river, towards Sieghilde's people, that would hopefully offer them some cover.

Antonio and Aldo discovered that people had been going through their things in the stable, but saddled up and headed north, in the general direction of Sighilde's people.  They could see that the fire was out in the inn, and that the dozen soldiers who had been outside had become a hive of activity headed north.

Eltharion, covering the group's escape, attacked a guard (Rochelle) from cover behind a tree, and she tumbled senseless to the river's edge.  Her friend, following closely, attacked the arm that struck his friend.  Attempting to parry, Eltharion overbalanced and tumbled into the rushing torrent, clinging to the rocky shore with only his head above water.

Elyse, at the back of those fleeing, is about a second ahead of the soldiers in pursuit.