---
title: "Along the River"
categories: whitemouse
card: "/images/card/raging-river.jpg"
image: "/images/banner/raging-river.jpg"
date: 2019-03-07
---

We began our episode with Antonio and Aldo, having asked directions of the guards, riding casually to the west, seeking the comfort of the Inn at the small town of Blackraven.  Although wet, their ride is leisurely with the promise of a warm, dry bed and a hot meal at the other end.

Eltharion's thrilling heroics from the last episode landed him in the river.  He hurled himself out of the water, over the rocks, and stabbed one of the soldiers intent on capturing him in the leg.

Further up the trail, Elyse pulled her whistle to play a Song of Terror.  Although everyone was agreed that they would not be requesting an encore, it did have the charm of terrifying the soldiers who had already been injured.  Three ran away, and one soldier who had fallen in the river let go of the rock she had been clinging to.

A dramatic running fight along the trail, in which Eltharion identified himself as a force of nature, whittled down Imhoff's soldiers, but also resulted in Sieghilde taking one heck of a sprawling fall down the path, and Elyse getting bounced off a few rocks herself.

A final standoff with a very intimidating Eltharion, and a withering "You're so ugly, you were four before you realized that you name wasn't 'Damn!'" gave Eltharion, Elyse, Saskia, and Sieghilde the chance to escape down the trail and up a hidden stair in the rock face.

Back in Blackraven, Aldo and Antonio found a nice dry bed at the Blackraven Inn.  The common room had three friends enjoying the very drinkable ale, and one fellow sitting in a corner by himself nursing a grudge.  As soon as Antonio joined Aldo at the table, Grudge sat down uninvited and without saying a word.
