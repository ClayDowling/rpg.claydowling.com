---
title: "White Mouse Characters"
tags: ["whitemouse"]
date: 2018-12-27
comments: false
---

The voting has concluded, and the choice of the masses is [The White Mouse]({% link _campaigns/white-mouse.md %}).  The system is [Dungeon Fantasy](http://www.sjgames.com/dungeonfantasy/).  This system is powered by GURPS, but the Dungeon Fantasy rules have some differences worth noting:

* Characters start at 250 points.
* Summoning spells are not part of the magic system.

There are others, those are just the ones off the top of my head.

## Your Character

While we are using Dungeon Fantasy rules, we are not constrained to a traditional dungeon fantasy game confined to killing monsters and taking their stuff.  The game we get will depend in large part on the characters you create.  We do *not* need to enforce niche protection, requiring certain roles within the party to be filled.  If we wind up with four bards, or four clerics, or four wizards, that's totally cool.  We'll run that kind of adventure.

What does this mean for you?  It means create the kind of character you want to create, within the rules of Dungeon Fantasy.  If you're not sure how to do that, send me a text and we'll work something out.

## Character Blogs

If you're interested in writing an in-character blog, the website software supports that.  Send me a text and we'll work out the details.  Writing blog posts is a lot easier with the new software compared to the old.

## Your Background

I'd love to know more about who your character is, and how they wound up in an inn waiting to cross a river, hoping to get a job selling their particular set of skills to a nobleman they have no fealty to.

If you'd like, I have a copy of *The Ultimate RPG Character Backstory Guide* which provides some fun exercises we can do as a group to flesh this out.