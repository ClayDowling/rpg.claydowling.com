---
title: "Martingale Security"
genre: "Near Future Science Fiction"
system: "GURPS Transhuman Space"
image: "/images/card/transhuman-space-launch.jpg"
---

In the year 2100, technology has advanced, but the need for security specialists has not changed from the 2010s when Hart Security was formed.  Rebranded to reflect changes in management (Aaron Hart's daughter, Olivia, married Howard Martingale, and her daughter took over day to day management of the company in 2087), the mission remains the same: help those whose needs aren't matched by formal law enforcement.

Using the [Transhuman Space](http://www.sjgames.com/gurps/transhuman/) setting, where artificial intelligences might be worn as jewelry, implanted in your skull, or roaming free on global computer networks, sublight travel to Mars and space colonies is common, and body modification is considered a sapient right, the world in very interesting, and provides a rich variety of characters to play, including modified humans, human animal chimeras, uplifted animals (dogs, octopi, and dolphins being most common), and free roaming artificial intelligences.