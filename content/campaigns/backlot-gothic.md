---
title: "Shadows Over Filmland"
genre: "1930s Backlot Gothic"
system: "Genesys or GUMSHOE"
image: "/images/card/shadows-over-filmland.jpg"
---

Inspired by the black and white horror films of the 1920s and 1930s, the players are investigators who for one reason or another are drawn into the mysterious world "East of Switzerland, west of Hell."  The emphasis is on atmosphere and suspense, rather than the more traditional mind bending madness inspired by understanding higher mathematics too well.

Imagine a world which the modern age hasn't quite reached. Where there are a few trappings of 1930s modernity, but only enough to remind you that 1930 is a long way away.

The goal is to create something akin to black and white serials, with adventures in mysterious, unnamed locales.  Heavily armed action heroes or bucklers of swashes need not apply.  One person armed with a modern revolver will be the most heavily armed person in an entire village.
