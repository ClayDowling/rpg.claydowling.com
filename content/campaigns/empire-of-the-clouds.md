---
title: "Empire of the Clouds"
image: "/images/card/shaz-abdullah-airship.jpg"
tags: ["gensys", "steampunk"]
system: Genesys
genre: Steampunk
---

The *Empire of the Clouds* is a marvel admired the world over.  Top scientists, artists, and engineers flock to her shores to create the greatest empire ever known.  Her airships fill the skys, carrying modern marvels of steam and electricity to foreign lands, and gold back to the Empire.

But there is rot at her core.  Comfortable nobles and ambitious merchants put personal gain against the welfare of the empire and her people.  You've bled to defend God and Empire, and you can't watch this rot continue unchecked.  

You are a band of former military, criminals, and common people who have found common cause against the moral rot that puts the boot of the wealthy and powerful on the neck of the poor and powerless.  

Impired by Iron Maiden's [Empire of the Clouds](https://www.youtube.com/watch?v=YbAhn7iKLPc).