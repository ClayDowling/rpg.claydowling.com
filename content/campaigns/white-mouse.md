---
title:  "The White Mouse"
image: "/images/card/sturgeon-river.jpg"
system: GURPS
genre: "High Fantasy"
---

The ads promised good pay for stout hearts who could help Baron Imhoff rid his lands of the goblin raiders. They failed to mention the heavy rains that have swollen rivers, stopped travel, and kept you holed up in the inn at the ferry crossing for the last three days.

When a woman and her chaperone arrive, and quietly insinuate themselves into your group, it doesn't raise too many eyebrows. When she asks you to say that she's been here for three days with you, that does raise some eyebrows. It is not lost on you that men wearing Imhoff's colors showed up twenty minutes after the lady.

The rules will be [GURPS Dungeon Fantasy](http://www.sjgames.com/dungeonfantasy/). The tone will be more one of mystery and intrigue in a high fantasy world. As for the monsters, well, we'll see who that turns out to be.