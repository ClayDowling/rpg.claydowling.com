---
title: "Hart Security"
image: "/images/card/duluth-ship.jpg"
system: GURPS
genre: "Modern Action"
---

We rejoin our heroes six months after where we left off.   Infiltration of the Lesbian Separatists is complete, and there's some kind of direct action in the works.  

We don't have the character sheets, so we'll create 400 point Action characters inspired by the original characters, or new characters to represent the staffing changes which inevitably happen in any organization.

The good news: The IRS has finished their audit of Hart finances, so the coffers are open again.