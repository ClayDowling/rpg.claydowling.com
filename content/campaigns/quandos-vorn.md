---
title: "Quandos Vorn Must Die"
system: "Gaean Reach"
genre: "Romanesque Space Fantasy"
image: "/images/card/quandos-vorn-must-die.jpg"
---

Across the vastness of human space a few individuals of exceptional infamy project their lust for power. Fear of their names spreads from planet to planet like a cancer.

None of these evokes greater loathing and terror than the world-spanning criminal mastermind Quandos Vorn.

**Quandos Vorn. Who you have sworn to kill.**

You are victims, or the survivors of victims, of Quandos Vorn. You have dedicated your life to ending his, no matter what lengths you must go to.

More details about the rule system can be found at [Gaena Reach](https://site.pelgranepress.com/index.php/the-gaean-reach-2/) by Pelgrane Press.