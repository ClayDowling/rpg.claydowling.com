---
title: "Diana/Diego Hughes Character Sheet"
weight: 100
---

300 points

Grizzled, rather battered, 52-year-old exmilitary (an early version genefixed human), neat in appearance but with unruly blond hair.

**ST** 11 [10]; **DX** 12 [40]; **IQ** 13 [60]; **HT** 11 [10].

Damage 1d-1/1d+1; BL 24 lbs.; HP 12 [2]; Will 13 [0]; Per 13 [0]; FP 11 [0].

Basic Speed 6.00 [5]; Basic Move 6 [0]; Dodge 10; Parry 11 (Brawling).

5’9”; 170 lbs.

## Social Background

TL: 10 [0].
CF: Western (Native) [0].
Languages: English (Native) [0]; Spanish (Native) [6].

## Advantages

* Ally (“Jack”; 100% of starting points; Constantly Available; Minion, +50%) [30];
* Ally (“Ratchet”; 75% of starting points; Constantly Available; Minion, +50%) [18];
* Born War-Leader* 3 [15];
* Combat Reflexes [15];
* Contact (Old Army Buddy in the Pentagon; Current Affairs (Politics)-18; 12 or less; Somewhat Reliable) [6];
* Filter Lungs (Lung Cleaner Nanosymbionts) [5];
* Fit [5];
* Immunity to Known Bacteria (Bacteriophage Nanosymbionts) [5];
* Immunity to Known Viruses (Virus Hunter Nanosymbionts) [5];
* Longevity (Nanosymbiont Treatments) [2];
* Regeneration (DNA Repair Nanosymbionts; Slow; Radiation Only, -60%) [4];
* Reputation +2 (A good security specialist, in the business; 10 or less) [1];
* Resistant to Ingested Poisons +8 (Liver Upgrade) [5];
* Resistant to Nanomachines +8 (Guardian Nanosymbionts) [2];
* Status 1 [0]†;
* Wealthy [20].

### Perks

* Alcohol Tolerance (Liver Upgrade). [1]

## Disadvantages

* Code of Honor (Professional Bodyguard) [-5];
* Extra Sleep 2 (Mitigator, Weekly Treatment, nanodrug therapy, -65%) [-1];
* Insomniac (Mild; Mitigator, Weekly Treatment, nanodrug therapy, -65%) [-3];
* Sense of Duty (Their team) [-5];
* Unattractive [-4].

### Quirks

* Can’t hide their enthusiasm in moments of high action; 
* Dislikes microbots; 
* Refuses to play politics; 
* Sincerely patriotic – wouldn’t touch a job that significantly threatened U.S. interests. [-4]

## Skills

Skill | Dif | Rel | Pts | Level
----- | --- | --- | --- | -----
Area Knowledge (Central America) | (E) | IQ | [1] | 13
Armoury/TL10 (Small Arms) | (A) | IQ-1 | [1] | 12
Battlesuit/TL10 | (A) | DX-1 | [1] | 11
Beam Weapons/TL10 (Pistol) | (E) | DX+1 | [2] | 13
Brawling | (E) | DX+2 | [4] | 14
Camouflage | (E) | IQ | [1] | 13
Cartography/TL10 | (A) | IQ-1 | [1] | 12
Driving/TL10 (Automobile) | (A) | DX | [2] | 12
Guns/TL10 (Gyroc) | (E) | DX | [1] | 12
Guns/TL10 (Pistol) | (E) | DX+1 | [1] | 13
Guns/TL10 (Rifle) | (E) | DX+2 | [4] | 14
Guns/TL10 (Shotgun) | (E) | DX | [0] | 12
Guns/TL10 (Submachine Gun) | (E) | DX | [0] | 12
History (21st Century Military) | (H) | IQ | [4] | 13
Housekeeping | (E) | IQ | [1] | 13
Intelligence Analysis/TL10 | (H) | IQ+1 | [1] | 14
Leadership | (A) | IQ+3 | [2] | 16
Navigation/TL10 (Land) | (A) | IQ-1 | [1] | 12
Observation | (A) | Per+2 | [8] | 15
Piloting/TL10 (Vertol) | (A) | DX | [2] | 12
Savoir-Faire (Military) | (E) | IQ+3 | [1] | 16
Savoir-Faire (Police) | (E) | IQ | [1] | 13
Soldier/TL10 | (A) | IQ | [2] | 13
Strategy (Land) | (H) | IQ+1 | [1] | 14
Streetwise | (A) | IQ-1 | [1] | 12
Survival (Desert) | (A) | Per-1 | [1] | 12
Survival (Jungle) | (A) | Per-1 | [1] | 12
Survival (Mountain) | (A) | Per-1 | [1] | 12
Survival (Plains) | (A) | Per-1 | [1] | 12
Tactics | (H) | IQ+1 | [1] | 14
Throwing | (A) | DX-1 | [1] | 11

## Equipment

Hughes is always equipped appropriately for the mission, and takes professional care of his gear. He usually wears “Jack” over a light full-body protective suit, and always has a “sunglasses” visor with HUD. He’s also old-fashioned enough to carry a sap in his pocket when possible, just in case that proves the best option.