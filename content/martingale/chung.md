---
title: "Portia/Paul Chung"
weight: 60
---

You were born in 2071, in British Columbia, into a Chinese-Canadian family who weren’t especially well off by local standards. It didn’t help your circumstances when your mother went down with skin cancer a few years back. She recovered, but it was rough on her, and it hit the family finances. It also made you very aware of those sorts of dangers (which is why you always wear a hat outdoors, and don’t forget stuff like sunscreen).  Your parents brought you up to be law-abiding, but you also decided that you wanted to make something more of yourself. Fortunately, you’ve always been tolerably bright and well-built, and you keep yourself in good condition, so you had a good start.

Your talents are kind of practical, certainly, and your first real job was with the Alberta and British Columbia national police. After a couple of years, they offered you a place with the border security unit. It was higher risk, but the deal included a bunch of seriously useful nanomods and biomods, including a full-on permanent brain booster suite, so you took it. After a few years more, though, you decided that the job wasn’t going anywhere fast enough or paying enough; your horizons had broadened. Therefore, you resigned the force on good terms, and you looked for something else.

With your experience, you found a job with Martingale Security, a new outfit just setting up in the United States. Bodyguarding doesn’t pay terribly well, but they were offering more than your old job. You figure that you should be able to strike out on your own in a couple of years, or maybe pick up a good corporate post. Martingale has decent resources, and you are getting to see the world this way.

You have decent police/security training, including experience in the wilds of Canada. You spend a lot of time on the virtual range, keeping your handgun skills up to scratch. Along with the brain booster, which takes your reaction times right up, you’ve had treatments that protect you against all sorts of natural and synthetic infections and attacks. Your physical abilities are innate, although you make sure that you keep up your training.

## Playing Paul

People sometimes think that you’re a bit taciturn--one or two appraisals even said "surly"--but you prefer to call it focused. Your job in the team is to hang back and provide the necessary firepower if things ever turn heavy. Charlie Mallinson stands in front, being visible and obviously a cybershell, and perhaps draws fire. Paz Ramirez sticks by the client, protecting him from minor hassles and getting him clear or down as appropriate. Your job is to be less visible than either of them, but to remain in sight, provide back-up, and deal with anything that needs serious force. You prefer to carry a couple of decent handguns, loaded for a variety of targets and ranges. Unfortunately, you can’t always get legal clearance for more sophisticated ammunition or heavier sidearms. You rarely have to worry about body armor or combat cybershells. Anyway, the point of the exercise is getting the client away safe, not fighting a war, and you focus on that.

[Chung's Sheet]({{< ref "chung-sheet.md" >}})

### Chung’s Views of the Team

You believe in standing by your teammates as a group. Fortunately, this lot isn’t too bad, but some of them are more professional than other members.

Hughes is in charge and signs off your paychecks. They're a grizzled, competent old army type, with a lived-in sort of face--their parents can’t have paid for their genes to be polished. They're a convincing enough team leader, though.

Ramirez is also ex-army. They has the job of providing the client with close cover. They really seems to enjoy the work, to a degree that grated on you a bit at first but that you’ve now got used to. Actually, to tell the truth, you find them kind of cute. It doesn’t hurt that they're a blonde and their parents evidently got them one of those high-end genetic fix-ups that makes them a bit of a looker, but you like them as a person; they really enjoys life. Nevertheless, it’d be unprofessional and dangerous to get distracted by those sorts of feelings in combat. You’re not sure how they feel about you, so you keep your mind on the job and don’t embarrass them by saying anything. Maybe some day....

Mallinson leaves you a bit less impressed. They're okay as a person, but they're no professional. It seems that they were some kind of office drone who got themselves uploaded to software (which probably means that they were getting on a bit--only weird transhumanist types go in for voluntary uploading when they’re young). Since they're now backed up, they decided that they could enjoy himself a bit more. Not that they should treat serious security work as a hobby, of course . . . Well, fortunately, they're not too stupid about it, and their attitude means that they don’t mind acting as point person. They try, and they've even given you some good investment advice.

Lastly, OVERSIGHT is Martingale’s office AI – fully sapient, and quite efficient, right up to doing three things at once. It has control of a bunch of miniature flying cybershells that give you a lot of eyes in the sky, which is useful. It’s a bit weird, of course--AIs aren’t human, even the advanced models like OVERSIGHT, and it acts a bit nosy and a bit pleased with itself--but you can live with it.

## Chung’S AI

Some people think that you don’t trust AIs, but that’s not true; you just get a bit twitchy about implanted computers. The idea of someone putting an AI inside their own head strikes you as creepy, though you know all the urban legends are rubbish. (Well, most of them. Maybe not all.) You do have an AI of your own, though, courtesy of your employers – a basic nonsapient thing, installed in a miniature computer on your belt. It feeds you information through an earpiece and a HUD in your armored shades, and you operate it mostly through a throat mike. It’s a handy information management tool, but its main job is running stuff like HUD-targeting and facial-recognition software. Its virtual avatar is a simplified humanoid mannequin.