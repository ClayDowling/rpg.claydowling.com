---
title: "Portia/Paul Chung Character Sheet"
weight: 110
---

300 points

A well-built mixed-race genefixed human with a somewhat
athletic figure; age 29.

**ST** 14 [40]; **DX** 12 [40]; **IQ** 12 [40]; **HT** 12 [20].

Damage 1d/2d; BL 39 lbs.; HP 14 [0]; Will 15 [15]; Per 12 [0]; FP 12 [0].

Basic Speed 6.00 [0]; Basic Move 7 [5]; Dodge 10; Parry 10 (Karate).

6’4”; 190 lbs.

Social Background

TL: 10 [0].

CF: Western (Native) [0]; Chinese [1].

Languages: English (Native) [0]; Cantonese (Native) [6]; French (Accented) [4]; Spanish (Native) [6].

## Advantages

* Ally (Wearable Tactical System; 25% of starting points; Constantly Available; Minion, +0%) [4];
* Attractive [4];
* Comfortable [10];
* Enhanced Time Sense (Brain Booster Nanosymbionts) [45];
* Filter Lungs (Lung Cleaner Nanosymbionts) [5];
* Resistant to Disease +3 (Immune Machine Nanosymbionts) [3];
* Resistant to Nanomachines +8 (Guardian Nanosymbionts) [2].

### Perks

* Honest Face. [1]

## Disadvantages

* Honesty (15) [-5];
* Odious Personal Habit (Excessively taciturn and rather dour) [-5];
* Sense of Duty (To the team) [-5].

### Quirks

* Congenial; 
* Determined to get rich by honest means;
* Nervous about strong sunlight – always wears a hat, shades, and sunscreen; 
* Secretly a bit infatuated with Ramirez;
* Somewhat suspicious of AIs running in implants. [-5]

## Skills

Skill | Dif | Rel | Pts | Level
----- | --- | --- | --- | -----
Acrobatics | (H) | DX-1 | [2] | 11
Acting | (A) | IQ-1 | [1] | 11
Area Knowledge (Alberta & British Columbia) | (E) | IQ | [1] | 12
Beam Weapons/TL10 (Pistol) | (E) | DX+3 | [7] | 15 
Criminology/TL10 | (A) | IQ+1 | [4] | 13 
Fast-Draw/TL10 (Ammo) | (E) | DX+1 | [1] | 13
Fast-Draw (Pistol) | (E) | DX+1 | [1] | 13
First Aid/TL10 (Human) | (E) | IQ+1 | [2] | 13 
Guns/TL10 (Gyroc) | (E) | DX+4 | [12] | 16
Guns/TL10 (Pistol) | (E) | DX+4 | [11] | 16 
Guns/TL10 (Submachine Gun) | (E) | DX+2 | [0] | 14 
Hiking | (A) | HT-1 | [1] | 11
Karate | (H) | DX | [4] | 12
Law (Alberta and British Columbia Police) | (H) | IQ | [4] | 12
Naturalist (Earth) | (H) | IQ-2 | [1] | 10
Navigation/TL10 (Land) | (A) | IQ-1 | [1] | 11 
Savoir-Faire (Police) | (E) | IQ | [1] | 12
Search | (A) | Per-1 | [1] | 11
Shadowing | (A) | IQ+2 | [8] | 14 
Stealth | (A) | DX+1 | [4] | 13
Streetwise | (A) | IQ-1 | [1] | 11
Survival (Woodlands) | (A) | Per-1 | [1] | 11

## Equipment

Chung usually wears decent but concealable body armor.
They favor heavy handgun-sized sidearms