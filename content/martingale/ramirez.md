---
title: "Paulo/Paz Ramirez"
weight: 70
---

You come from a rather ordinary background--born and raised in New Mexico--but you decided a while back that you were going to see the world and see some action. You also enjoy a good party, to an extent that makes some boring people turn disapproving. (You don’t let how you feel the next morning stop you either, though you ought to see if there’s a treatment for that some day.)

One of your early choices was to join the army and get yourself some front-line action training. You even managed to swing yourself a permanent brain booster nanomod, some anti-radiation treatments, and other stuff. You’ve also splashed out on getting some other modifications, which is why you have good coordination as well as these neat glittery nictitating membranes on your eyes.

Once in, you discovered that the army mostly uses cybershells for the interesting stuff these days. Without a war on, you mustered out after a few years, and went looking for something more reliably stimulating. Private security work seemed like a good application of your talents. You eventually hooked a job with Martingale Security--a new one-team outfit that uses real human staff because some people find armed cybershells too intimidating. Your job is to keep clients safe--and you can look good while you’re doing that, if necessary.

You were born with Alpha Upgrade genetics, and your investments and Army treatments have built on that. You’re resistant or immune to most diseases, nanomachine attacks, and pollutants. You can take more radiation than an unmodified human. Your eyes are visibly enhanced, your heart will keep going when many would give up, and you don’t have body odor problems. Moreover, you have broad military and security training.

## Playing Paz

You’re someone who believes in living life--real life, not some virtual game--to the full, and looking good while you’re at it. You don’t let that desire interfere with doing your job, and you like to do it properly.

Your function in this team is to remain close to the client. Most people, if they look around the area, spot that blatant cyberdoll Mallinson and think of him as "the bodyguard"; you are a little less conspicuous. Some people still tag you as the key figure, thanks to the well-cut dark suit and the shades. That’s fine, because you should be able to hold off serious problems for long enough for Chung to drop them. Of course, that’s a worst case: Usually, you just deck the odd minor trouble-maker or Mallinson slows things down while you get the client into cover. While you enjoy action, you are a professional, and you’re paid to keep the client alive.

Incidentally, your parents were paid-up members of the Sons and Daughters of the King (the Elder), also known as the Reformed Presleyan Heresy or the Elder Elvis Schism.