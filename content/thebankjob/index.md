---
title: "The Bank Job"
date: 2019-12-22T16:18:52-05:00
---

Please Choose a Character

* [Brittaney Manemann](BrittaneyManemann.pdf) / [Bret Manemann](BretManemann.pdf) Face / Confidence Artists
* [Julia Ostasiewicz](JuliaOstasiewicz.pdf) / [Julius Ostasiewicz](JuliusOstasiewicz.pdf) Demolition Woman/Man
* [Winnie Zhao](WinnieZhao.pdf) / [Winston Zhao](WinstonZhao.pdf) Fast Girl/Guy Martial Artists
* [Celeste Corkill](CelesteCorkill.pdf) / [Charlie Corkill](CharlieCorkill.pdf) Shooters
* [Kareem Degollado](KareemDegollado.pdf) / [Karen Degollado](KarenDegollado.pdf) Cleaners / Evidence Removal
* [Woodrow Mertes](WoodrowMertes.pdf) / [Wendy Mertes](WendyMertes.pdf) Wheel Man/Woman
* [Gustavo Cervenka](GustavoCervenka.pdf) / [Guinnevere Cervenka](GuinnevereCervenka.pdf) Assasins
* [Leonel Sharber](LeonelSharber.pdf) / [Lauren Sharber](LaurenSharber.pdf) Infiltrator / Burglar
* [Jarrett Cabriales](JarrettCabriales.pdf) / [Janice Cabriales](JaniceCabriales.pdf) Investigator
* [Shelton Gash](SheltonGash.pdf) / [Shelly Gash](ShellyGash.pdf) Definitely not ninjas, which don't even exist

          
           
  
     
