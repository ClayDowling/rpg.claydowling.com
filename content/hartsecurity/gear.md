---
title: "Gear"
date: 2021-02-20T13:21:09-05:00
---

# Software

* [Audacity](https://www.audacityteam.org/)  You will get significantly better audio by recording your audio on your computer.  That will work better than trying to record the Discord.

# Microphones

* [Fifine Microphone](https://amzn.to/3dy2tyX) This is a $35 USB microphone.  It's a good reliable microphone that is easy to use.

* [Maono Microphone Kit](https://amzn.to/3aD7vIX)  Although it costs more money, it's a wonderful microphone, especially if you need to work online or record video or audio for work.  It's actually slightly easier to use than the Fifine microphone.

# Headphones

* [AKG K240](https://amzn.to/3qQR4Ow) These are pro-level headphones that are comfortable to wear for hours on end.

* [Sony Headphone](https://amzn.to/3qFlxPL) Dirt cheap, and I haven't used them, but I've had good luck with Sony phones over the years.

Nearly any ear buds or headphones will work.  The most important thing is that they're comfortable and won't leak sound into your microphone.
