---
title: "Characters"
date: 2020-02-01T10:51:13-05:00
---

The goal should be to recreate your characters from the GURPS Hart Security
campaign, as closely as you can in the Genesys system.  It's worth keeping in
mind that Genesys is made for a very different kind of game than GURPS, so
you're really looking for the spirit of the character rather than a perfect
mirroring of abilities.  Given that, feel free to deviate from the GURPS
character as you think is necessary, or just because you think that it's
interesting.  It's been many years since we played, and there's no need to be
sticklers.

## Points

After you pick an archetype, you will find how many points you have to start
building your characters.  Add another 50 points to the starting value for your
archetype.  These characters were definitely larger than life, even at the
start of Hart Security.

## Equipment

Gear can be taken from the Modern Day equipment from the [Genesys Rule Book](https://amzn.to/2Srrb8x).  These are experienced operators, so rarity should not be a reason to discount an item.  If in doubt, ask, and we'll work out something that will fit the game.

### Additional Weapons

These are here because the game master has an unhealthy interested in knives.

| Name        | Skill | Dam | Crit | Range   | Encum | Price | Rarity |
|-------------|-------|-----|------|---------|-------|-------|--------|
| Folding Knife | Melee | +1 | 5   | Engaged | 0     | 20    | 1      |
| Switch Blade | Melee | +1 | 4    | Engaged | 0     | 100   | 4      |
| Small Knife | Melee | +1  | 4    | Engaged | 1     | 30    | 1      |
| Large Knife | Melee | +2  | 3    | Engaged | 1     | 100   | 1      |

**Folding Knife** A folding pocket knife, suitable for every day carry.  They may have a sheath that fits on a belt, like the Buck 110, or a clip to hold them on a pocket, like the Kershaw Leek, or just slip quietly into a pocket, like the Case knives.

**Switch Blade** A folding knife which deploys the blade with the press of a button.  Legality varies widely, and it's never a good idea to be caught with one by law enforcement.  Often sold as "Automatic" knives.  These knives have a tendency to bite their owners when deploying

**Small Knife** A fixed blade knife which requires a sheath to carry.  These are typically but not always utilitarian tools with easy to hold handles and a guard or choil to keep your hand from sliding onto the blade in the event during a thrust.  The blade is under 6".

**Large Knife** A larger version of the small knife.  It almost certainly has a guard.  The blade will be 6" or longer.  Generally but not always legal.
