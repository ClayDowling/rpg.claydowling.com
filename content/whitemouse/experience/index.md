---
title: White Mouse Experience
author: Clay Dowling
---

| Character | XP  |
| --------- | --- |
| aldo      | 437 |
| antonio   | 447 |
| eltharion | 456 |
| elyse     | 439 |
