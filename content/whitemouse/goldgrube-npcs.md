---
title: "Goldgrube NPCs"
date: 2020-06-25
---

Frederick - Young acolyte from the healing temple

[Carla Fairburne](https://images.squarespace-cdn.com/content/v1/5a735f7dd7bdceaa03dafee4/1557507852550-SRQG8T9VFGJ1PHRD4YAS/ke17ZwdGBToddI8pDm48kC4EXbe1lHngxuPXFcJdZF57gQa3H78H3Y0txjaiv_0fDoOvxcdMmMKkDsyUqMSsMWxHk725yiiHCCLfrh8O1z5QPOohDIaIeljMHgDF5CVlOqpeNLcJ80NK65_fV7S1UcN16mDF27kuwLc8zh_1BUDBOQ46HaP-qJ8HsER-o3kDi8k3Sy4pRENMTFdprkci1g/Period%2BCorsets%2Bcustom%2BRen%2BFaire%2Bensemble) - Pretty, friendly, bar maid at Dicke Wiltes's.

[Hannelore Heitz](https://hips.hearstapps.com/digitalspyuk.cdnds.net/12/31/cult_merlin_morgana.JPG) - Friendly beauty from the bar.

[Phillip Hardt](https://cosmicbook.news/sites/default/files/styles/image_header/public/henry-cavill-responds-superman-no-longer-relevant.jpg?itok=PTWnB7_j) Stone Mason.  The particular friend of Wilfried.

[Wilfried Noll](https://www.scotsman.com/images-e.jpimedia.uk/imagefetch/http://www.scotsman.com/webimage/Prestige.Item.1.70941425!image/image.jpg?width=640)  Farmer, owns a farm out of the way south of Goldsgrub.  The particular friend of Phillip.
