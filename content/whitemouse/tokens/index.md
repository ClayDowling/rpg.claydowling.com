---
title: "Tokens"
date: 2021-08-19T21:39:55-04:00
---

I created the following tokens for use in mapping tools.  If you would like a different token, go to the [2-Minute Tabletop Token Editor](https://tools.2minutetabletop.com/) and generate your own.  It's a lot of fun.

{{< figure src="Aldo.png" title="Aldo" >}}

{{< figure src="Antonio.png" title="Antonio" >}}

{{< figure src="Eltharion.png" title="Eltharion" >}}

{{< figure src="Elyse.png" title="Elyse" >}}

