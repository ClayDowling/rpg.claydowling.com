<?xml version="1.0" encoding="US-ASCII" ?>
<character id="9eec0daf-0b6e-4caa-9bd8-918404432ed7" version="3">
	<created_date>Feb 3, 2019</created_date>
	<modified_date>Feb 3, 2019, 8:38 PM</modified_date>
	<profile>
		<player_name>clay</player_name>
		<campaign>White Mouse</campaign>
		<name>Trooper Sargeant</name>
		<age>27</age>
		<birthday>April 23</birthday>
		<eyes>Brown</eyes>
		<hair>Black, Straight, Medium</hair>
		<skin>Brown</skin>
		<handedness>Right</handedness>
		<height>5' 6"</height>
		<weight>170 lb</weight>
		<SM>0</SM>
		<gender>Male</gender>
		<race>Human</race>
		<body_type>humanoid</body_type>
		<tech_level>3</tech_level>
	</profile>
	<HP>0</HP>
	<FP>0</FP>
	<total_points>100</total_points>
	<ST>11</ST>
	<DX>13</DX>
	<IQ>11</IQ>
	<HT>11</HT>
	<will>0</will>
	<perception>0</perception>
	<speed>0</speed>
	<move>0</move>
	<include_punch>true</include_punch>
	<include_kick>true</include_kick>
	<include_kick_with_boots>true</include_kick_with_boots>
	<advantage_list>
		<advantage version="2">
			<name>Rank</name>
			<type>Physical</type>
			<levels>3</levels>
			<points_per_level>5</points_per_level>
			<modifier version="1" enabled="no">
				<name>Replaces Status</name>
				<cost type="points">5</cost>
				<affects>levels only</affects>
				<reference>B29</reference>
			</modifier>
			<modifier version="1" enabled="no">
				<name>Courtesy</name>
				<cost type="points">-4</cost>
				<affects>levels only</affects>
				<reference>B29</reference>
			</modifier>
			<reference>B29</reference>
			<notes>Military: Sargeant</notes>
			<categories>
				<category>Advantage</category>
			</categories>
		</advantage>
		<advantage version="2">
			<name>Combat Reflexes</name>
			<type>Mental</type>
			<base_points>15</base_points>
			<reference>B43</reference>
			<notes>Never freeze; +6 on all IQ rolls to wake up or to recover from surprise or mental stun; Your side gets +1 to initiative rolls (+2 if you're the leader)</notes>
			<categories>
				<category>Advantage</category>
			</categories>
			<skill_bonus>
				<name compare="starts with">fast-draw</name>
				<specialization compare="is anything"></specialization>
				<amount>1</amount>
			</skill_bonus>
			<attribute_bonus>
				<attribute>dodge</attribute>
				<amount>1</amount>
			</attribute_bonus>
			<attribute_bonus>
				<attribute>parry</attribute>
				<amount>1</amount>
			</attribute_bonus>
			<attribute_bonus>
				<attribute>block</attribute>
				<amount>1</amount>
			</attribute_bonus>
			<attribute_bonus>
				<attribute>fright check</attribute>
				<amount>2</amount>
			</attribute_bonus>
			<prereq_list all="yes">
				<advantage_prereq has="no">
					<name compare="is">Enhanced Time Sense</name>
					<notes compare="is anything"></notes>
				</advantage_prereq>
			</prereq_list>
		</advantage>
		<advantage version="2">
			<name>Sense of Duty</name>
			<type>Mental</type>
			<modifier version="1" enabled="no">
				<name>Friends and Companions</name>
				<cost type="points">-5</cost>
				<affects>total</affects>
				<reference>B153</reference>
			</modifier>
			<modifier version="1">
				<name>Their Platoon</name>
				<cost type="points">-5</cost>
				<affects>total</affects>
				<reference>B153</reference>
			</modifier>
			<modifier version="1" enabled="no">
				<name>@Individual@</name>
				<cost type="points">-2</cost>
				<affects>total</affects>
				<reference>B153</reference>
			</modifier>
			<modifier version="1" enabled="no">
				<name>@Large Group@</name>
				<cost type="points">-10</cost>
				<affects>total</affects>
				<reference>B153</reference>
			</modifier>
			<modifier version="1" enabled="no">
				<name>@Entire Race@</name>
				<cost type="points">-15</cost>
				<affects>total</affects>
				<reference>B153</reference>
			</modifier>
			<modifier version="1" enabled="no">
				<name>Every Living Being</name>
				<cost type="points">-20</cost>
				<affects>total</affects>
				<reference>B153</reference>
			</modifier>
			<reference>B153</reference>
			<categories>
				<category>Disadvantage</category>
			</categories>
		</advantage>
		<advantage version="2">
			<name>Duty (Lord Imhoff)</name>
			<type>Social</type>
			<modifier version="1" enabled="no">
				<name>FR: 6</name>
				<cost type="points">-2</cost>
				<affects>total</affects>
			</modifier>
			<modifier version="1" enabled="no">
				<name>FR: 9</name>
				<cost type="points">-5</cost>
				<affects>total</affects>
			</modifier>
			<modifier version="1">
				<name>FR: 12</name>
				<cost type="points">-10</cost>
				<affects>total</affects>
			</modifier>
			<modifier version="1" enabled="no">
				<name>FR: 15</name>
				<cost type="points">-15</cost>
				<affects>total</affects>
			</modifier>
			<modifier version="1" enabled="no">
				<name>Extremely Hazardous</name>
				<cost type="points">-5</cost>
				<affects>total</affects>
			</modifier>
			<modifier version="1" enabled="no">
				<name>Involuntary</name>
				<cost type="points">-5</cost>
				<affects>total</affects>
			</modifier>
			<modifier version="1" enabled="no">
				<name>Nonhazardous</name>
				<cost type="points">5</cost>
				<affects>total</affects>
			</modifier>
			<reference>B133</reference>
			<categories>
				<category>Disadvantage</category>
			</categories>
		</advantage>
		<advantage version="2">
			<name>Bad Temper</name>
			<type>Mental</type>
			<base_points>-10</base_points>
			<cr>12</cr>
			<reference>B124</reference>
			<categories>
				<category>Disadvantage</category>
			</categories>
		</advantage>
		<advantage version="2">
			<name>Code of Honor (Soldier's)</name>
			<type>Mental</type>
			<base_points>-10</base_points>
			<reference>B127</reference>
			<categories>
				<category>Disadvantage</category>
			</categories>
		</advantage>
	</advantage_list>
	<skill_list>
		<technique version="2" limit="5">
			<name>Disarming</name>
			<difficulty>H</difficulty>
			<points>2</points>
			<reference>B230</reference>
			<default>
				<type>Skill</type>
				<name>Broadsword</name>
				<modifier>0</modifier>
			</default>
			<categories>
				<category>Combat/Weapon</category>
				<category>Melee Combat</category>
				<category>Technique</category>
			</categories>
		</technique>
		<technique version="2" limit="5">
			<name>Retain Weapon</name>
			<difficulty>H</difficulty>
			<points>2</points>
			<reference>B232</reference>
			<default>
				<type>Skill</type>
				<name>Broadsword</name>
				<modifier>0</modifier>
			</default>
			<categories>
				<category>Combat/Weapon</category>
				<category>Melee Combat</category>
				<category>Technique</category>
			</categories>
		</technique>
		<skill version="2">
			<name>Broadsword</name>
			<difficulty>DX/A</difficulty>
			<points>8</points>
			<reference>B208</reference>
			<categories>
				<category>Combat/Weapon</category>
				<category>Melee Combat</category>
			</categories>
			<default>
				<type>Skill</type>
				<name>Force Sword</name>
				<modifier>-4</modifier>
			</default>
			<default>
				<type>Skill</type>
				<name>Rapier</name>
				<modifier>-4</modifier>
			</default>
			<default>
				<type>Skill</type>
				<name>Saber</name>
				<modifier>-4</modifier>
			</default>
			<default>
				<type>Skill</type>
				<name>Shortsword</name>
				<modifier>-2</modifier>
			</default>
			<default>
				<type>Skill</type>
				<name>Two-Handed Sword</name>
				<modifier>-4</modifier>
			</default>
			<default>
				<type>DX</type>
				<modifier>-5</modifier>
			</default>
		</skill>
		<skill version="2">
			<name>Savoir-Faire</name>
			<specialization>Military</specialization>
			<difficulty>IQ/E</difficulty>
			<points>8</points>
			<reference>B218</reference>
			<categories>
				<category>Knowledge</category>
				<category>Military</category>
				<category>Social</category>
			</categories>
			<default>
				<type>IQ</type>
				<modifier>-4</modifier>
			</default>
		</skill>
		<skill version="2">
			<name>Knife</name>
			<difficulty>DX/E</difficulty>
			<points>1</points>
			<reference>B208</reference>
			<categories>
				<category>Combat/Weapon</category>
				<category>Melee Combat</category>
			</categories>
			<default>
				<type>Skill</type>
				<name>Force Sword</name>
				<modifier>-3</modifier>
			</default>
			<default>
				<type>Skill</type>
				<name>Main-Gauche</name>
				<modifier>-3</modifier>
			</default>
			<default>
				<type>Skill</type>
				<name>Shortsword</name>
				<modifier>-3</modifier>
			</default>
			<default>
				<type>DX</type>
				<modifier>-4</modifier>
			</default>
		</skill>
		<skill version="2">
			<name>Leadership</name>
			<difficulty>IQ/A</difficulty>
			<points>2</points>
			<reference>B204</reference>
			<categories>
				<category>Military</category>
				<category>Social</category>
			</categories>
			<default>
				<type>IQ</type>
				<modifier>-5</modifier>
			</default>
		</skill>
		<skill version="2">
			<name>Tactics</name>
			<difficulty>IQ/H</difficulty>
			<points>1</points>
			<reference>B224</reference>
			<categories>
				<category>Military</category>
				<category>Police</category>
			</categories>
			<default>
				<type>IQ</type>
				<modifier>-6</modifier>
			</default>
			<default>
				<type>Skill</type>
				<name>Strategy</name>
				<modifier>-6</modifier>
			</default>
		</skill>
	</skill_list>
	<equipment_list>
		<equipment version="4" state="equipped">
			<quantity>1</quantity>
			<description>Broadsword</description>
			<tech_level>2</tech_level>
			<legality_class>4</legality_class>
			<value>500</value>
			<weight>3 lb</weight>
			<reference>B271</reference>
			<melee_weapon>
				<damage>sw+1 cut</damage>
				<strength>10</strength>
				<usage>Swung</usage>
				<reach>1</reach>
				<parry>0</parry>
				<block>No</block>
				<default>
					<type>DX</type>
					<modifier>-5</modifier>
				</default>
				<default>
					<type>Skill</type>
					<name>Force Sword</name>
					<modifier>-4</modifier>
				</default>
				<default>
					<type>Skill</type>
					<name>Broadsword</name>
					<modifier>0</modifier>
				</default>
				<default>
					<type>Skill</type>
					<name>Rapier</name>
					<modifier>-4</modifier>
				</default>
				<default>
					<type>Skill</type>
					<name>Saber</name>
					<modifier>-4</modifier>
				</default>
				<default>
					<type>Skill</type>
					<name>Shortsword</name>
					<modifier>-2</modifier>
				</default>
				<default>
					<type>Skill</type>
					<name>Two-Handed Sword</name>
					<modifier>-4</modifier>
				</default>
				<default>
					<type>Skill</type>
					<name>Sword!</name>
					<modifier>0</modifier>
				</default>
			</melee_weapon>
			<melee_weapon>
				<damage>thr+1 cr</damage>
				<strength>10</strength>
				<usage>Thrust</usage>
				<reach>1</reach>
				<parry>0</parry>
				<block>No</block>
				<default>
					<type>DX</type>
					<modifier>-5</modifier>
				</default>
				<default>
					<type>Skill</type>
					<name>Force Sword</name>
					<modifier>-4</modifier>
				</default>
				<default>
					<type>Skill</type>
					<name>Broadsword</name>
					<modifier>0</modifier>
				</default>
				<default>
					<type>Skill</type>
					<name>Rapier</name>
					<modifier>-4</modifier>
				</default>
				<default>
					<type>Skill</type>
					<name>Saber</name>
					<modifier>-4</modifier>
				</default>
				<default>
					<type>Skill</type>
					<name>Shortsword</name>
					<modifier>-2</modifier>
				</default>
				<default>
					<type>Skill</type>
					<name>Two-Handed Sword</name>
					<modifier>-4</modifier>
				</default>
				<default>
					<type>Skill</type>
					<name>Sword!</name>
					<modifier>0</modifier>
				</default>
			</melee_weapon>
			<categories>
				<category>Melee Weapon</category>
			</categories>
		</equipment>
		<equipment version="4" state="equipped">
			<quantity>1</quantity>
			<description>Large Knife</description>
			<tech_level>0</tech_level>
			<legality_class>4</legality_class>
			<value>40</value>
			<weight>1 lb</weight>
			<reference>B272</reference>
			<melee_weapon>
				<damage>sw-2 cut</damage>
				<strength>6</strength>
				<usage>Swung</usage>
				<reach>C,1</reach>
				<parry>-1</parry>
				<block>No</block>
				<default>
					<type>DX</type>
					<modifier>-4</modifier>
				</default>
				<default>
					<type>Skill</type>
					<name>Knife</name>
					<modifier>0</modifier>
				</default>
				<default>
					<type>Skill</type>
					<name>Force Sword</name>
					<modifier>-3</modifier>
				</default>
				<default>
					<type>Skill</type>
					<name>Main-Gauche</name>
					<modifier>-3</modifier>
				</default>
				<default>
					<type>Skill</type>
					<name>Shortsword</name>
					<modifier>-3</modifier>
				</default>
				<default>
					<type>Skill</type>
					<name>Sword!</name>
					<modifier>0</modifier>
				</default>
			</melee_weapon>
			<melee_weapon>
				<damage>thr imp</damage>
				<strength>6</strength>
				<usage>Thrust</usage>
				<reach>C</reach>
				<parry>-1</parry>
				<block>No</block>
				<default>
					<type>DX</type>
					<modifier>-4</modifier>
				</default>
				<default>
					<type>Skill</type>
					<name>Knife</name>
					<modifier>0</modifier>
				</default>
				<default>
					<type>Skill</type>
					<name>Force Sword</name>
					<modifier>-3</modifier>
				</default>
				<default>
					<type>Skill</type>
					<name>Main-Gauche</name>
					<modifier>-3</modifier>
				</default>
				<default>
					<type>Skill</type>
					<name>Shortsword</name>
					<modifier>-3</modifier>
				</default>
				<default>
					<type>Skill</type>
					<name>Sword!</name>
					<modifier>0</modifier>
				</default>
			</melee_weapon>
			<ranged_weapon>
				<damage>thr imp</damage>
				<strength>6</strength>
				<usage>Thrown</usage>
				<accuracy>+0</accuracy>
				<range>x0.8/x1.5</range>
				<rate_of_fire>1</rate_of_fire>
				<shots>T(1)</shots>
				<bulk>-2</bulk>
				<default>
					<type>DX</type>
					<modifier>-4</modifier>
				</default>
				<default>
					<type>Skill</type>
					<name>Thrown Weapon</name>
					<specialization>Knife</specialization>
					<modifier>0</modifier>
				</default>
			</ranged_weapon>
			<categories>
				<category>Melee Weapon</category>
				<category>Missile Weapon</category>
			</categories>
		</equipment>
		<equipment version="4" state="equipped">
			<quantity>1</quantity>
			<description>Mail Shirt</description>
			<tech_level>2</tech_level>
			<legality_class>4</legality_class>
			<value>150</value>
			<weight>16 lb</weight>
			<reference>B283</reference>
			<notes>DR 4</notes>
			<categories>
				<category>Body Armor</category>
			</categories>
			<dr_bonus>
				<location>torso</location>
				<amount>4</amount>
			</dr_bonus>
		</equipment>
	</equipment_list>
	<print_settings printer="HP LaserJet Professional M1212nf MFP" units="in">
		<orientation>portrait</orientation>
		<width>8.5</width>
		<height>11</height>
		<top_margin>0.5</top_margin>
		<left_margin>0.5</left_margin>
		<bottom_margin>0.5</bottom_margin>
		<right_margin>0.5</right_margin>
		<ink_chromaticity>monochrome</ink_chromaticity>
		<sides>single</sides>
		<quality>normal</quality>
	</print_settings>
</character>
