---
title: "NPC Names"
---

## Male Names

- Hademar Dannecker
- Theobald Kipping
- Ulrich Göring
- Gunthard Hasselvander
- ~~August Apfelbaum~~
- Hildebrand Schützenberger
- Ubaldo Hirtreiter
- ~~Berthold Dinter~~
- Jannick Kaltenbrunner
- Gundolf Hoenigsberg

## Female Names

- Wiltrud Spanner
- Roswitha Blumenkrantz
- Luitgard Kirchweger
- Ella Kampf
- ~~Asgard Steuermann~~
- Lamberta Buchholz
- Charlotte Hahnemann
- ~~Saskia Sulzer~~
- ~~Sieghilde Weitz~~
- Gunda Creuzburg
