---
title: "Flusskasserne"
date: 2019-09-04T20:21:57-04:00
card: "/images/card/flusskasserne.jpg"
tags:
- whitemouse
- map
---

The layout of the Flusskasserne compound, as best as the PCs are aware.

{{< figure src="/images/flusskasserne.jpg" link="/images/flusskasserne.jpg" target="_new" caption="The fort at River Run" >}}
